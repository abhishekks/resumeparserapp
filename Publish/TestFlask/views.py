"""
Routes and views for the flask application.
"""
import os, shutil
import zipfile
import glob
from datetime import datetime
from datetime import datetime
from flask import Flask, render_template, request,make_response,send_from_directory,send_file
from TestFlask import app
from flask_dropzone import Dropzone
import pandas as pa

from .ParsePdfFiles import *

basedir = os.path.abspath(os.path.dirname(__file__))

app.config.update(
    UPLOADED_PATH=os.path.join(basedir, 'uploads'),
    DOWNLOAD_PATH=os.path.join(basedir, 'downloads'),
    SPLIT_FILE_UPLOAD_PATH=os.path.join(basedir, 'splitfileupload'),
    SPLITFILE_PATH=os.path.join(basedir, 'splitfiles'),
    # Flask-Dropzone config:
    #DROPZONE_DEFAULT_MESSAGE='Drop the file here to parse',
    DROPZONE_ALLOWED_FILE_CUSTOM = True,
    DROPZONE_ALLOWED_FILE_TYPE='.pdf',
    DROPZONE_MAX_FILE_SIZE=100,
    #DROPZONE_MAX_FILES=1,
    #DROPZONE_UPLOAD_ON_CLICK=True
    #DROPZONE_REDIRECT_VIEW='download'
)

bookmarksDict=dict()
dropzone = Dropzone(app)
dfNewCandidateDetails= pa.DataFrame()


@app.route('/', methods=['POST', 'GET'])
@app.route('/home')
def home():
    if request.method == 'POST':
        myuploaddir=os.path.join(app.config['UPLOADED_PATH'])

        if os.path.exists(myuploaddir):
            print("Exists !")
            shutil.rmtree(myuploaddir)
            os.mkdir(myuploaddir)
        else:
            os.mkdir(myuploaddir)

        mydownloaddir=os.path.join(app.config['DOWNLOAD_PATH'])

        if os.path.exists(mydownloaddir):
            print("Exists !")
            shutil.rmtree(mydownloaddir)
            os.mkdir(mydownloaddir)
        else:
            os.mkdir(mydownloaddir)
       
        for key, f in request.files.items():
            if key.startswith('file'):
                f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))


    return render_template('index.html',title='Home')

@app.route('/download')
def download():
    try:
        FILE_NAME=''
        for root, dirs, files in os.walk(os.path.join(app.config['UPLOADED_PATH'])):
            for filename in files:
                print(filename)
                file_path=os.path.join(app.config['UPLOADED_PATH'], filename)
                FILE_NAME=filename
                bookmarksDict=parse_bookmarks_using_PyPDF2(file_path)

                dfNewCandidateDetails=extract_table_contents(file_path,bookmarksDict)
            
                #download_the_CSV(dfNewCandidateDetails,filename)
                #downloadFileName=save_the_CSV(dfNewCandidateDetails,FILE_NAME)
                downloadFileName=save_the_Excel(dfNewCandidateDetails,FILE_NAME)

                listSerialNumbers=list(dfNewCandidateDetails['Serial_No'])
            
                intlistSerialNumber=[int(x) for x in listSerialNumbers]
            
                # Finding missing elements in List 
                res = list(sorted(set(range(1, intlistSerialNumber[-1])) - set(intlistSerialNumber))) 


        return render_template(
            'download.html',
            title=FILE_NAME,
            message='Candidates data',
            missingDataFor=str(res),
            tables=[dfNewCandidateDetails.to_html(classes=["table table-bordered", "table-striped", "table-hover"], header="true",index=False)],
            filename=downloadFileName 
        )
    except Exception as e:
                 return render_template('error.html',title='Error',error=str(e))

@app.route('/contact')
def contact():
    """Renders the contact page."""
    contactlistJson = [{
            "eid":"123",
            "name": "Abhishek K S",
            "Email": "abhishek@example.com"
        },
        {
            "eid":"235",
            "name": "Madhu R",
            "Email": "madhu@example.com"
        },
        {
            "eid":"856",
            "name": "Bharath H R",
            "Email": "bharath@example.com"
        }]
    return render_template(
        'contact.html',
        title='Contact details',
        year=datetime.now().year,
        result=contactlistJson
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About the Resume Parser App',
        year=datetime.now().year
    )

@app.route('/downloadFile/<path:filename>')
def downloadFile(filename):
    downloads = os.path.join(app.config['DOWNLOAD_PATH'])
    return send_from_directory(directory=downloads, filename=filename,as_attachment=True)

#@app.route('/', methods=['POST', 'GET'])
@app.route('/split',methods=['POST', 'GET'])
def split():
    try:
        if request.method == 'GET':

            mysplitfiledir=os.path.join(app.config['SPLITFILE_PATH'])

            if os.path.exists(mysplitfiledir):
                print("Exists !")
                shutil.rmtree(mysplitfiledir)
                os.mkdir(mysplitfiledir)
                print("Created SPLITFILE_PATH !")
            else:
                os.mkdir(mysplitfiledir)
                print("Created SPLITFILE_PATH !")

            mysplitfileUploaddir=os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'])

            if os.path.exists(mysplitfileUploaddir):
                print("Exists !")
                shutil.rmtree(mysplitfileUploaddir)
                os.mkdir(mysplitfileUploaddir)
                print("Created SPLIT_FILE_UPLOAD_PATH !")
            else:
                os.mkdir(mysplitfileUploaddir)
                print("Created SPLIT_FILE_UPLOAD_PATH !")

        if request.method == 'POST':
            for key, f in request.files.items():
                if key.startswith('file'):
                    f.save(os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'], f.filename))
                    print("Created file for Split {}".format(f.filename))
                    print("Created path of SplitFolder {}".format(os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'], f.filename)))

        return render_template('split.html',title='Split File')
    except Exception as e:
                 return render_template('error.html',title='Error',error=str(e))

def save_the_CSV(dfNewCandidateDetails,filename):
   print(dfNewCandidateDetails.dtypes)
   downloadFilePath=os.path.join(app.config['DOWNLOAD_PATH'], filename);
   downloadFilePathCSV=os.path.splitext(downloadFilePath)[0]+".csv";
   dfNewCandidateDetails.to_csv(downloadFilePathCSV,index = None, header=True)
   return os.path.splitext(filename)[0]+".csv"

def save_the_Excel(dfNewCandidateDetails,filename):
   print(dfNewCandidateDetails.dtypes)
   downloadFilePath=os.path.join(app.config['DOWNLOAD_PATH'], filename);
   downloadFilePathCSV=os.path.splitext(downloadFilePath)[0]+".xlsx";
   with pa.ExcelWriter(downloadFilePathCSV) as writer:
       dfNewCandidateDetails.to_excel(writer,engine='xlsxwriter',index = None, header=True)
   return os.path.splitext(filename)[0]+".xlsx"

@app.route('/splitpdffile')
def splitpdffile():
    FILE_NAME=''
    ZIP_FILE=''

    mydownloaddir=os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'])
    filedownloadlist = glob.glob(os.path.join(mydownloaddir, "*.*"))
 
    # handle errors while calling os.remove()
    #try:
    #    for f in filedownloadlist:
    #        print("Name of the file in SPLIT_FILE_UPLOAD_PATH ", f)
    #except:
    #    print("error while deleting file ", f)

    for root, dirs, files in os.walk(os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'])):
        for filename in files:
            file_path=os.path.join(app.config['SPLIT_FILE_UPLOAD_PATH'], filename)
            FILE_NAME=filename
            try:
                bookmarksDict=parse_bookmarks_using_PyPDF2(file_path)
            
                #print("Length of the dictionary : {}".format(len(bookmarksDict)))

                pdf_splitter(file_path,filename,bookmarksDict)
            
            except Exception as e:
                 return render_template('error.html',title='Error',error=str(e))

    #        #ziparchive(filename)
            #output_filename=os.path.join(basedir,'splitfiles')
            #dir_name=os.path.join(basedir,'splitfiles')
            #newdirectory=os.path.join(dir_name,os.path.splitext(filename)[0])
            #shutil.make_archive(newdirectory, 'zip', newdirectory)
            
            #print("Created Zip file archieve !")

            #zip_file = '{}.zip'.format(newdirectory)
    ##        ZIP_FILE=zip_file
    ##        print("Zip file info {}".format(ZIP_FILE))
    ##return send_file(
    ##          ZIP_FILE,
	   ##       mimetype='application/zip',
    ##          as_attachment=True,
    ##          attachment_filename='{}.zip'.format(os.path.splitext(FILE_NAME)[0]))
            #filedownloadlist = glob.glob(os.path.join(newdirectory, "*.*"))
            #data = io.BytesIO()
            #with zipfile.ZipFile(data, mode='w',compression=zipfile.ZIP_DEFLATED) as z:
            #    for f_name in filedownloadlist:
            #        z.write(f_name)
            #data.seek(0)
    #return send_file(
    #    zip_file,
    #    mimetype='application/zip',
    #    as_attachment=True,
    #    attachment_filename='{}.zip'.format(os.path.splitext(FILE_NAME)[0])
    #)
    return render_template(
        'zipdownload.html',
        filename=os.path.splitext(FILE_NAME)[0] 
    )

@app.route('/ziparchive/<path:filename>')
def ziparchive(filename):
    output_filename=os.path.join(basedir,'splitfiles')
    dir_name=os.path.join(basedir,'splitfiles')
    newdirectory=os.path.join(dir_name,filename)

    # Call the function to retrieve all files and folders of the assigned directory
    #filePaths = retrieve_file_paths(dir_name)
    ##dir_name = newdirectory
    ## printing the list of all files to be zipped
    #print('The following list of files will be zipped:')
    #mydownloaddir=os.path.join(app.config['SPLITFILE_PATH'])
    #filedownloadlist = glob.glob(os.path.join(mydownloaddir, "*.*"))
 
    # handle errors while calling os.remove()
    #try:
    #    for f in filedownloadlist:
    #        print("Name of the file in SPLIT_FILE_UPLOAD_PATH ", f)
    #except:
    #    print("error while deleting file ", f)
    #for fileName in filePaths:
    #    print(fileName)
     
  # writing files to a zipfile
    #data = io.BytesIO()
    #with zipfile.ZipFile(data, mode='w') as z:
    #    for file in filePaths:
    #        z.write(file,os.path.basename(file))

    #data.seek(0)
    #zip_archive = zipfile.ZipFile(filename+'.zip', "r")
    ## list file information
    #for file_info in zip_archive.infolist():
    #    print(file_info.filename, file_info.date_time, file_info.file_size)

    #with zip_file:
    ## writing each file one by one
    #    for file in filePaths:
    #        zip_file.write(file)
       

    shutil.make_archive(newdirectory, 'zip', newdirectory)
    zip_file = '{}.zip'.format(newdirectory)

    return send_file(
      zip_file,
	  mimetype='application/zip',
      as_attachment=True,
      attachment_filename='{}.zip'.format(filename))
    #return send_from_directory(directory=downloads, filename=filename,as_attachment=True)

# Declare the function to return all file paths of the particular directory
def retrieve_file_paths(dirName):
 
  # setup file paths variable
    filePaths = []
    for files in glob.glob(os.path.join(dirName, "*.*")):
       filePaths.append(files)
  # Read all directory, subdirectories and file lists
    #for root, directories, files in os.walk(dirName):
    #    for filename in files:
    #    # Create the full filepath by using os module.
    #        filePath = os.path.join(root, filename)
    #        filePaths.append(filePath)
         
  # return all paths
    return filePaths