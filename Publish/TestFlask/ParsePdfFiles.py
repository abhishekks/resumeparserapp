import PyPDF2
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import io
import re
from tabula import read_pdf
import pandas as pa
import numpy as np
from fuzzywuzzy import fuzz
import os
from TestFlask import app

#https://stackoverflow.com/questions/54303318/read-all-bookmarks-from-a-pdf-document-and-create-a-dictionary-with-pagenumber-a
def parse_bookmarks_using_PyPDF2(FILE_PATH):

    with open(FILE_PATH, mode='rb') as f:
        reader = PyPDF2.PdfFileReader(f,strict=False)
        bookmarksDict= bookmark_dict(reader.getOutlines(),reader)
    return bookmarksDict


def bookmark_dict(bookmark_list,reader):
    result = {}
    for item in bookmark_list:
        if isinstance(item, list):
            # recursive call
            result.update(bookmark_dict(item,reader))
        else:
            result[reader.getDestinationPageNumber(item)] = item.title
    return result

def extract_table_contents(pdf_path,bookmarksDict):
    # Read pdf into DataFrame
    fileTable = read_pdf(pdf_path, pages=(str(1)+'-'+str(next(iter(bookmarksDict)))),index_col=0)
    df=pa.DataFrame(fileTable)
    
    df.dropna(axis=1, thresh=int(0.9*len(df)),inplace=True)
    df = df.dropna()
    df = df.reset_index(drop=True)
    print(df.columns)
    print(df.dtypes)
    print(df.head())
    df["Serial_No"]= df["Candidate Name"].str.replace('[^0-9\s]+', '').str.strip()
    df["Candidate_Name"]=df["Candidate Name"].str.replace('[^A-Za-z\s]+', '').str.strip()
    #df.rename( columns={'Unnamed: 1':'Candidate_ID'}, inplace=True )
    df.rename( columns={'Candidate ID':'Candidate_ID'}, inplace=True )
    #df["Candidate_ID"]=df["Candidate_ID"].astype(int)
    df.drop('Candidate Name', axis=1, inplace=True)
    print(df.head())
    dfcolumnsTitles = ['Serial_No', 'Candidate_Name', 'Candidate_ID']
    df = df.reindex(columns=dfcolumnsTitles)
    df.drop(df[df['Candidate_Name'] == 'Candidate Name'].index, inplace = True) 
    print(df.head())
    candidateNameList=list(df["Candidate_Name"])
    return extract_text_from_pdf(pdf_path,candidateNameList,bookmarksDict,df)


def extract_text_from_pdf(pdf_path,dfcandidateNameList,bookmarksDict,df):
    with open(pdf_path, 'rb') as fh:
        reader = PyPDF2.PdfFileReader(fh,strict=False)
         # creating a resoure manager
        resource_manager = PDFResourceManager()
            
            # create a file handle
        fake_file_handle = io.StringIO()
            
            # creating a text converter object
        converter = TextConverter(resource_manager, 
                                fake_file_handle, 
                                codec='utf-8', 
                                laparams=LAParams())

            # creating a page interpreter
        page_interpreter = PDFPageInterpreter(resource_manager, 
                                converter)

        # iterate over all pages of PDF document
        text=''
        count=0
        bookmarkKeyList=sorted(bookmarksDict.keys())
        #df["Page_No"]=bookmarkKeyList
        dfcandidateIDlist=df["Candidate_ID"]
        dfserialNoList=df["Serial_No"]
        serialNo=[]
        candidateNamelist=[]
        candidateIDlist=[]
        mobileNumberlist=[]
        emailIDlist=[]

        splits=list(bookmarksDict.keys())
        count=0
        start=splits[count]
        end=splits[count+1]

        for pageNumber,page in enumerate(PDFPage.get_pages(fh, caching=True, check_extractable=True)):

            #if (pageNumber >=next(iter(bookmarksDict))):
            if(pageNumber==start and (pageNumber) in range(start,end)):          
                bookMarkName=bookmarksDict.get(start,"NotFound")
            # process current page
                page_interpreter.process_page(page)
            
            # extract text
                text = fake_file_handle.getvalue()
                #bookMarkName=bookmarksDict.get(pageNumber,"NotFound")
               
                if (((fuzz.partial_ratio(text.strip().lower(),bookMarkName.split(" ")[0].lower())>90) or
                         (fuzz.token_set_ratio(text.strip().lower(),bookMarkName.split(" ")[0].lower())>90) or 
                         (fuzz.partial_ratio(text.strip().replace(" ","").lower(),bookMarkName.split(" ")[0].lower())>90) or 
                         (fuzz.token_set_ratio(text.strip().replace("\n","").lower(),bookMarkName.split(" ")[0].lower())>90))):
                    
                    print("Count :",count+1)
                    serialNo.append(list(dfserialNoList)[count])
                   
                    print("Name :",bookMarkName)
                    candidateNamelist.append(bookMarkName)
                    
                    candidateID=list(dfcandidateIDlist)[count]
                    print("Candidate ID",candidateID)
                    candidateIDlist.append(candidateID)
                    
                    MobNumber=extract_mobile_number(text)
                    mobileNumberlist.append(MobNumber)
                    print("Mobile Number :",MobNumber)
                    
                    EmailId=extract_email(text)
                    emailIDlist.append(EmailId)
                    print("Email ID :",EmailId)
                    
                    #increment the count
                    #count+=1
                
                text=''
                fake_file_handle.truncate(0)
                fake_file_handle.seek(0)

            if(pageNumber==end-1):
                start=end
                try: 
                # setting split end position for next split
                    count+=1
                    end = splits[count+1] 
                except IndexError: 
                # setting split end position for last split 
                    end = reader.numPages
        #return text
                #text+=text
            # close open handles
                

        converter.close()
        fake_file_handle.close()
        
        dfNewcolumns=["Serial_No","Candidate_Name","Candidate_ID","Mobile_Number","Email_ID"]
        dfNewCandidateDetails=pa.DataFrame(columns=dfNewcolumns)


        dfNewCandidateDetails["Serial_No"]= serialNo
        dfNewCandidateDetails["Candidate_Name"]=candidateNamelist
        dfNewCandidateDetails["Candidate_ID"]=candidateIDlist
        dfNewCandidateDetails["Mobile_Number"]=mobileNumberlist
        dfNewCandidateDetails["Mobile_Number"]=dfNewCandidateDetails["Mobile_Number"].astype(str)
        dfNewCandidateDetails["Email_ID"]=emailIDlist
        return dfNewCandidateDetails
        #dfNewCandidateDetails.to_csv(r'C:\Users\jv194e\Desktop\Resumes\Outer.csv', index=False)
        #clean_dataframes(df,dfNewCandidateDetails)
        #return text

def extract_mobile_number(text):
    phone = re.findall(re.compile(r'(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?'), text)
    
    if phone:
        number = ''.join(phone[0])
        if len(number) > 10:
            return '+' + number
        else:
            return number

def extract_email(email):
    email = re.findall("([^@|\s]+@[^@]+\.[^@|\s]+)", email)
    if email:
        try:
            return email[0].split()[0].strip(';')
        except IndexError:
            return None

def pdf_splitter(FILE_PATH,filename,bookmarksDict):
    basedir = os.path.abspath(os.path.dirname(__file__))
    dirsplitfiles = os.path.join(basedir,r'splitfiles')
    #newdirectory=os.path.join(app.config['SPLITFILE_PATH'])
    newdirectory=os.path.join(dirsplitfiles,os.path.splitext(filename)[0])
    if os.path.exists(newdirectory):
        print("Exist {}".format(newdirectory))
    else:
        os.mkdir(newdirectory)
        print("Created {}".format(newdirectory))
    splits=list(bookmarksDict.keys())
    count=0
    start=splits[count]
    end=splits[count+1]

    with open(FILE_PATH, mode='rb') as f:
        reader = PyPDF2.PdfFileReader(f,strict=False)
    #pdf = PyPDF2.PdfFileReader(path)
        #splits=list(bookmarksDict.keys())
        #count=0
        #start=splits[count]
        #end=splits[count+1]

        for i in splits:
            pdf_writer = PyPDF2.PdfFileWriter()
            for page in range(start,end):
                pdf_writer.addPage(reader.getPage(page))
 
                output_filename='{}\\{}.pdf'.format(newdirectory, bookmarksDict.get(start))
 
            with open(output_filename, 'wb') as out:
                pdf_writer.write(out)
                 # interchanging page split start position for next split\
            print('Created {}: {}'.format(count+1,bookmarksDict.get(start))) 

            start = end
            try: 
                # setting split end position for next split
                count+=1
                end = splits[count+1] 
            except IndexError: 
                # setting split end position for last split 
                end = reader.numPages

    return FILE_PATH
        