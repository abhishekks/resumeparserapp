@echo off 
set curr_directory=%cd%
echo %curr_directory%
@PushD %curr_directory%\venv\Scripts"
@Call "activate.bat"
cd  ..\..
echo Preparing to run the application
@python runserver.py runserver
@PopD
pause