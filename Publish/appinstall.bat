@echo off 
REM echo %cd%
set curr_directory=%cd%
echo %curr_directory%
REM @python -m pip install --upgrade pip
@call virtualenv venv
echo Preparing to upgrade the Pip
@PushD %curr_directory%\venv\Scripts"
@Call "activate.bat"
cd  ..\..
echo Preparing to install requirements file, this may take some time
@PIP install -r requirements.txt
@pip3 install openpyxl
echo install requirements completed
echo Start to run the application
@python runserver.py runserver
@PopD
pause