# README #

* ### This is the Resume Parser application wriiten in **Python and Flask** to extract the candidate name,candidate number, mobile number and email. ###
* **It also Splits the each candidate resume when all candidates resume in a single PDF file**.   

To make the life easy I created to batch files.  

1. **appinstall.bat** : This creates the virtual environment for this application and install all the dependencies which are mentioned in requirements.txt .
2. **apprun.bat**     : This batch file starts running the application. Copy paste the http url from the command promt into the browser.  

**For better experience use Google chrome or Firefox**

### What are the screens it got? ###

* Home page
* Split page
* Contact page

### Home page ###

* Drag and drop/click on the box to upload the resume file.
* After uploading the file the app will process through the file to extarct Candidate Names,Candidate ID,Mobile number and Email ID and display it in HTML(Web) page.
* User can download this data into Excel sheet by clicking on button **Download to Excel**


### Split page ###

* Drag and drop/click on the box to upload the resume file.
* After uploading the file the app will process through the file split the resumes of individual candidates and downloads the same in **Zip file** automatically.


### Contact page ###

* Contains list of **Resume Parser App** developers details.
* Contact those developers if you need help related to this application.